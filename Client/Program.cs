﻿using DatabaseConnectionLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using Model;
using Configuration;
using System.Configuration;

namespace Client
{
    class Program
    {
        static List<Dictionary<string,object>> GetDataToInsert()
        {
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            Dictionary<string, object> dataRow = new Dictionary<string, object>();

            DateTime date = new DateTime(1976, 3, 21);

            dataRow.Add("name", "'Technical school'");
            dataRow.Add("address", "'Plovdiv'");
            dataRow.Add("date_of_foundation","'" + date.Year + "-" + date.Month + "-" + date.Day + "'");

            data.Add(dataRow);

            return data;
        }
        static Dictionary<string, object> GetDataToUpdate()
        {
            Dictionary<string, object> newRow = new Dictionary<string, object>();
            DateTime date = new DateTime(1965, 2, 3);

            newRow.Add("name", "'Math school'");
            newRow.Add("address", "'Blagoevgrad'");
            newRow.Add("date_of_foundation", "'" + date.Year + "-" + date.Month + "-" + date.Day + "'");

            return newRow;

        }

        static void Main(string[] args)
        {
            //IDataAdapter dataAdapter = new DatabaseDataAdapter();
            //dataAdapter.SetSource("Data Source = LRUZHINSKIAT; Initial Catalog = SchoolSystem; Integrated Security = true;");
            //IDataFacade dataFacade = new DatabaseDataFacade(dataAdapter);

            //List<Student> students = dataFacade.GetTableData<Student>(new DataQuery { Id = -1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            //Student toAdd = new Student();
            //toAdd.FirstName = "'Mitko'";
            //toAdd.MiddleName = "'Petkov'";
            //toAdd.LastName = "'Petkov'";
            //toAdd.DateOfBirth = new DateTime(2013, 3, 1);
            //toAdd.ClassId = 2;

            ////dataFacade.AddObject<Student>(toAdd, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            //dataFacade.DeleteObject(new DataQuery { Id = 25 }, DbStructure.STUDENT_TABLE_NAME);
            //dataFacade.ModifyObject<Student>(toAdd, new DataQuery { Id = 26 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            var config = TableMappingConfig.GetConfig();

            foreach(TableMapping item in config.TableMappings)
            {
                Console.WriteLine(item.TableName);
                foreach(Pair pair in item)
                {
                    Console.WriteLine(pair.DataSource + "->" + pair.Model);
                }
            }
        }
    }
}
