﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ActivityStudent
    {
        public int ActivityId { get; set; }
        public int StudentId { get; set; }
    }
}
