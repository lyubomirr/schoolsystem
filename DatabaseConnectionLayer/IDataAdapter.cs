﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnectionLayer
{
    public interface IDataAdapter
    {
        void SetSource(string connectionString);
        bool AddData(string tableName, List<Dictionary<string, object>> data);
        List<Dictionary<string, object>> GetData(string tableName, DataQuery query);
        bool UpdateData(string tableName, Dictionary<string, object> newData, DataQuery query);
        bool DeleteData(string tableName, DataQuery query);
       // List<string> GetTableColumns(string tableName);
        int GetLastIdSaved(string tableName);
    }

    public class DataQuery
    {
        public int Id { get; set; }
    }
}
