﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnectionLayer
{
    public class DatabaseDataAdapter : IDataAdapter
    {
        private string _connectionString;

        public bool AddData(string tableName, List<Dictionary<string, object>> data)
        {
            int rowsAffected = 0;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                foreach (Dictionary<string, object> row in data)
                {
                    int count = row.Count;

                    string[] keys = new string[count];
                    string[] values = new string[count];


                    for (int i = 0; i < count; i++)
                    {
                        keys[i] = row.ElementAt(i).Key;
                        values[i] = row.ElementAt(i).Value.ToString();
                    }

                    string keysJoined = "(" + String.Join(",", keys) + ")";
                    string valuesJoined = "(" + String.Join(",", values) + ")";

                    string cmdText = $"INSERT INTO {tableName} {keysJoined} VALUES {valuesJoined};";

                    using (SqlCommand command = new SqlCommand(cmdText, conn))
                    {
                        rowsAffected += command.ExecuteNonQuery();
                    }
                }
            }

            return rowsAffected > 0;
        }

        public bool DeleteData(string tableName, DataQuery query)
        {
            string cmdTxt = $"DELETE FROM {tableName} WHERE id={query.Id}";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand(cmdTxt, conn))
                {
                    return command.ExecuteNonQuery() > 0;
                }
            }           
        }

        public List<Dictionary<string, object>> GetData(string tableName, DataQuery query)
        {
            DataTable table = new DataTable(tableName);
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            string cmdText;
            if(query?.Id > 0)
            {
                cmdText = $"SELECT * FROM {tableName} WHERE id={query.Id};";
            }
            else
            {
                cmdText = $"SELECT * FROM {tableName};";
            }

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(table);
                }
            }

            foreach (DataRow row in table.Rows)
            {
                Dictionary<string, object> currentRow = new Dictionary<string, object>();

                foreach (DataColumn column in table.Columns)
                {
                    currentRow.Add(column.ToString(), row[column]);
                }

                data.Add(currentRow);
            }

            return data;
        }

        public void SetSource(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool UpdateData(string tableName, Dictionary<string, object> newData, DataQuery query)
        {
            int rowsAffected = 0;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                string cmdTxt =$"UPDATE {tableName} SET ";
                string vals = "";

                foreach(KeyValuePair<string, object> item in newData)
                {
                    vals += (item.Key + "=" + item.Value + ", ");
                }

                vals = vals.Remove(vals.Length - 2);
                cmdTxt += vals;
                cmdTxt += $" WHERE id={query.Id}";

                using (SqlCommand command = new SqlCommand(cmdTxt, conn))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
            }

            return rowsAffected > 0;
        } 

        public int GetLastIdSaved(string tableName)
        {

            DataTable data = new DataTable(tableName);
            string cmdText = $"SELECT MAX(id) FROM {tableName}";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(data);
                }
            }

            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn column in data.Columns)
                {
                    return (int)row[column];
                }
            }

            throw new InvalidOperationException("Nothing saved to table! ");
        }
    }
}
