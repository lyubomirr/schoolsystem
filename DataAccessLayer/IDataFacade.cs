﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DatabaseConnectionLayer;

namespace DataAccessLayer
{
    public interface IDataFacade
    {
        List<ModelT> GetTableData<ModelT>(DataQuery query, string tableName, Tuple<string, string>[] tableColumns) where ModelT : class, new();
        bool DeleteObject(DataQuery query, string tableName);
        bool ModifyObject<ModelT>(ModelT modified, DataQuery query, string tableName, Tuple<string, string>[] tableColumns) where ModelT: class;
        bool AddObject<ModelT>(ModelT newObject, string tableName, Tuple<string, string>[] tableColumns) where ModelT : class;
    }
}
