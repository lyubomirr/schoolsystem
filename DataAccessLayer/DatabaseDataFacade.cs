﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DatabaseConnectionLayer;
using Model;

namespace DataAccessLayer
{
    public class DatabaseDataFacade : IDataFacade
    {
        private IDataAdapter _dataAdapter;

        public DatabaseDataFacade(IDataAdapter dataAdapter)
        {
            _dataAdapter = dataAdapter;
        }

        private Dictionary<string, object> constructObjectRawData<ModelT>(Type type, Tuple<string,string>[] tableColumns, ModelT originalObject) where ModelT: class
        {
            Dictionary<string, object> parsedObj = new Dictionary<string, object>();
            foreach (var mapping in tableColumns)
            {
                if (mapping.Item1 != "id")
                {
                    if (type.GetProperty(mapping.Item2).PropertyType == typeof(DateTime))
                    {
                        DateTime date = (DateTime)type.GetProperty(mapping.Item2).GetValue(originalObject);
                        parsedObj.Add(mapping.Item1, "'" + date.ToString("yyyy-MM-dd") + "'");
                    }
                    else
                    {
                        parsedObj.Add(mapping.Item1, type.GetProperty(mapping.Item2).GetValue(originalObject));
                    }
                }
            }

            return parsedObj;
        }

        public bool AddObject<ModelT>(ModelT newObject, string tableName, Tuple<string, string>[] tableColumns) where ModelT: class
        {
            Type type = newObject.GetType();
            Dictionary<string, object> parsedObj = constructObjectRawData(type, tableColumns, newObject);

            if (_dataAdapter.AddData(tableName, new List<Dictionary<string, object>>() { parsedObj }))
            {
                if(type.GetProperty("Id") != null)
                {
                    type.GetProperty("Id").SetValue(newObject, _dataAdapter.GetLastIdSaved(tableName));
                }
                return true;
            }

            return false;
        }
        public bool DeleteObject(DataQuery query, string tableName)
        {
            return _dataAdapter.DeleteData(tableName, query);
        }
        public List<ModelT> GetTableData<ModelT>(DataQuery query, string tableName, Tuple<string,string>[] tableColumns) where ModelT : class, new()
        {
            List<ModelT> result = new List<ModelT>();
            List<Dictionary<string, object>> rawData = _dataAdapter.GetData(tableName, query);

            foreach (Dictionary<string, object> row in rawData)
            {
                ModelT single = new ModelT();
                Type type = single.GetType();

                foreach (var mapping in tableColumns)
                {

                    if(row.TryGetValue(mapping.Item1, out object rowValue))
                    {
                        if(type.GetProperty(mapping.Item2).PropertyType == typeof(DateTime))
                        {
                            string value = rowValue.ToString().Replace("'", "");
                            type.GetProperty(mapping.Item2).SetValue(single, DateTime.Parse(value));
                        }
                        else
                        {
                            type.GetProperty(mapping.Item2).SetValue(single, rowValue);
                        }
                    }
                }

                result.Add(single);
            }

            return result;
        }
        public bool ModifyObject<ModelT>(ModelT modified, DataQuery query, string tableName, Tuple<string,string>[] tableColumns) where ModelT: class
        {
            Type type = modified.GetType();
            Dictionary<string, object> parsedObj = constructObjectRawData(type, tableColumns, modified);
            return _dataAdapter.UpdateData(tableName, parsedObj, query);
        }
    }
}
