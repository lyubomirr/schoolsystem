﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public static class DbStructure
    {
        public const string STUDENT_TABLE_NAME = "Students";
        public const string TEACHER_TABLE_NAME = "Teachers";
        public const string ACTIVITY_TABLE_NAME = "Activites";
        public const string SCHOOL_TABLE_NAME = "Schools";
        public const string SCHOOL_CLASS_TABLE_NAME = "Classes";
        public const string ACTIVITY_STUDENT_TABLE_NAME = "ActivityStudents";

        public static Tuple<string, string>[] StudentColumns { get; private set; }
        public static Tuple<string, string>[] TeacherColumns { get; private set; }
        public static Tuple<string, string>[] SchoolClassColumns { get; private set; }
        public static Tuple<string, string>[] SchoolColumns { get; private set; }
        public static Tuple<string, string>[] ActivityColumns { get; private set; }
        public static Tuple<string, string>[] ActivityStudentColumns { get; private set; }


        static DbStructure()
        {
            StudentColumns = new Tuple<string, string>[] 
            {
                new Tuple<string, string>("id", "Id"),
                new Tuple<string, string>("date_of_birth", "DateOfBirth"),
                new Tuple<string, string>("first_name", "FirstName"),
                new Tuple<string, string>("middle_name", "MiddleName"),
                new Tuple<string, string>("last_name", "LastName"),
                new Tuple<string, string>("class_id", "ClassId")       
            };

            TeacherColumns = new Tuple<string, string>[]
            {
                new Tuple<string, string>("id", "Id"),
                new Tuple<string, string>("date_of_birth", "DateOfBirth"),
                new Tuple<string, string>("first_name", "FirstName"),
                new Tuple<string, string>("middle_name", "MiddleName"),
                new Tuple<string, string>("last_name", "LastName")
            };

            SchoolClassColumns = new Tuple<string, string>[]
            {
                new Tuple<string, string>("id", "Id"),
                new Tuple<string, string>("name", "Name"),
                new Tuple<string, string>("teacher_id", "TeacherId"),
                new Tuple<string, string>("school_id", "SchoolId")
            };

            SchoolColumns = new Tuple<string, string>[]
            {
                new Tuple<string, string>("id", "Id"),
                new Tuple<string, string>("name", "Name"),
                new Tuple<string, string>("adress", "Address"),
                new Tuple<string, string>("date_of_foundation", "DateOfFoundation")
            };

            ActivityColumns = new Tuple<string, string>[]
            {
                new Tuple<string, string>("id", "Id"),
                new Tuple<string, string>("name", "Name"),
                new Tuple<string, string>("teacher_id", "TeacherId")
            };

            ActivityStudentColumns = new Tuple<string, string>[]
            {
                new Tuple<string, string>("activity_id", "ActivityId"),
                new Tuple<string, string>("student_id", "StudentID")
            };
        }  
    }
}
