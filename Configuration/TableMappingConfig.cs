﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Configuration
{
    public class TableMappingConfig
         : ConfigurationSection
    {

        public static TableMappingConfig GetConfig()
        {
            return (TableMappingConfig)ConfigurationManager.GetSection("TableMappingConfig");
        }

        [System.Configuration.ConfigurationProperty("TableMappings")]
        [ConfigurationCollection(typeof(TableMapping), AddItemName = "TableMapping")]
        public TableMappings TableMappings
        {
            get
            {
               return (TableMappings) this["TableMappings"];

            }
        }

    }
}
