﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Configuration
{
    public class TableMappings : ConfigurationElementCollection
    {
        public TableMapping this[int index]
        {
            get
            {
                return base.BaseGet(index) as TableMapping;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new TableMapping this[string responseString]
        {
            get { return (TableMapping)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new TableMapping();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TableMapping)element).TableName;
        }
    }
}
