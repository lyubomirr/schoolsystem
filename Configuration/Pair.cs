﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Configuration
{
    public class Pair : ConfigurationElement
    {
        [ConfigurationProperty("Model", IsRequired = true)]
        public string Model
        {
            get
            {
                return (string)this["Model"];
            }
        }

        [ConfigurationProperty("DataSource", IsRequired = true)]
        public string DataSource
        {
            get
            {
                return (string)this["DataSource"];
            }
        }
    }
}
