﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace Configuration
{   
    public class TableMapping : ConfigurationElementCollection
    {

        [ConfigurationProperty("TableName", IsRequired = true)]
        public string TableName
        {
            get
            {
                return (string)base["TableName"];
            }
        }
        public Pair this[int index]
        {
            get
            {
                return base.BaseGet(index) as Pair;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new Pair this[string responseString]
        {
            get { return (Pair)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Pair();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Pair)element).DataSource;
        }
    }
}
