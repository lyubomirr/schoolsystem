﻿using DatabaseConnectionLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    internal class DataAdapterMock : IDataAdapter
    {
        private Dictionary<string, List<Dictionary<string, object>>> tableNameDataMap = new Dictionary<string, List<Dictionary<string, object>>>();
        private Dictionary<string, int> tableIndexesMap = new Dictionary<string, int>(); 

        public DataAdapterMock()
        {
            tableNameDataMap.Add("Students", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("Teachers", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("SchoolClasses", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("Schools", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("ActivityStudents", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("Activites", new List<Dictionary<string, object>>());

            foreach(KeyValuePair<string, List<Dictionary<string, object>>> table in tableNameDataMap)
            {
                tableIndexesMap.Add(table.Key, 0);
            }
        }


        public bool AddData(string tableName, List<Dictionary<string, object>> data)
        {
            if(data.Count > 0)
            {
                foreach(Dictionary<string, object> singleEntry in data)
                {
                    singleEntry.Add("id", ++tableIndexesMap[tableName]);
                    tableNameDataMap[tableName].Add(singleEntry);
                }
                return true;
            }

            return false;
        }

        public bool DeleteData(string tableName, DataQuery query)
        {
            foreach(Dictionary<string, object> row in tableNameDataMap[tableName])
            {
                if((int)row["id"] == query.Id)
                {
                    tableNameDataMap[tableName].Remove(row);
                    return true;
                }
            }

            return false;
        }

        public List<Dictionary<string, object>> GetData(string tableName, DataQuery query)
        {
            List<Dictionary<string, object>> returnedData = new List<Dictionary<string, object>>();

            if (query.Id > 0)
            {
                foreach (Dictionary<string, object> row in tableNameDataMap[tableName])
                {
                    if((int)row["id"] == query.Id)
                    {
                        return new List<Dictionary<string, object>>() { row };
                    }
                }

                return returnedData;
            }
            else
            {
                return tableNameDataMap[tableName];
            }
        }

        public int GetLastIdSaved(string tableName)
        {
            return tableIndexesMap[tableName];
        }

        public void SetSource(string connectionString)
        {
            throw new NotImplementedException();
        }

        public bool UpdateData(string tableName, Dictionary<string, object> newData, DataQuery query)
        {
            int count = tableNameDataMap[tableName].Count;
            newData.Add("id", GetLastIdSaved(tableName));

            for (int i = 0; i < count; i++)
            {
                if((int)tableNameDataMap[tableName][i]["id"] == query.Id)
                {
                    tableNameDataMap[tableName][i] = newData;
                    return true;
                }
            }

            return false;
        }
    }
}
