﻿using System;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using DatabaseConnectionLayer;
using System.Collections.Generic;

namespace UnitTests
{

    [TestClass]
    public class DataAdapterTests
    {
        private void clearTable(string tableName)
        {
            string cmdText = $"DELETE FROM {tableName};";

            try
            {
                using (SqlConnection conn = new SqlConnection(testDbConnectionString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(cmdText, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private void clearDb()
        {
            clearTable(DbStructure.ACTIVITY_STUDENT_TABLE_NAME);
            clearTable(DbStructure.ACTIVITY_TABLE_NAME);
            clearTable(DbStructure.STUDENT_TABLE_NAME);
            clearTable(DbStructure.SCHOOL_CLASS_TABLE_NAME);
            clearTable(DbStructure.SCHOOL_TABLE_NAME);
            clearTable(DbStructure.TEACHER_TABLE_NAME);
        }

        private string testDbConnectionString = "Data Source = LRUZHINSKIAT; Initial Catalog = SchoolSystemTest; Integrated Security = true;";
        private Dictionary<string, object> getMockedStudent()
        {
            Dictionary<string, object> mockedStudent = new Dictionary<string, object>();

            mockedStudent.Add("date_of_birth", "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            mockedStudent.Add("first_name", "'Ivan'");
            mockedStudent.Add("middle_name", "'Ivanov'");
            mockedStudent.Add("last_name", "'Petkov'");
            mockedStudent.Add("class_id", "1");

            return mockedStudent;
        }   
        private Dictionary<string, object> getMockedClass()
        {
            Dictionary<string, object> mockedClass = new Dictionary<string, object>();
            mockedClass.Add("name", "'2A'");
            mockedClass.Add("teacher_id", "1");
            mockedClass.Add("school_id", "1");

            return mockedClass;
        }
        private Dictionary<string, object> getMockedTeacher()
        {
            Dictionary<string, object> mockedTeacher = new Dictionary<string, object>();
            DateTime date = DateTime.Now;

            mockedTeacher.Add("date_of_birth", "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            mockedTeacher.Add("first_name", "'Ivan'");
            mockedTeacher.Add("middle_name", "'Ivanov'");
            mockedTeacher.Add("last_name", "'Petkov'");

            return mockedTeacher;
        }
        private Dictionary<string, object> getMockedSchool()
        {
            Dictionary<string, object> mockedSchool = new Dictionary<string, object>();
            mockedSchool.Add("name", "'PMG'");
            mockedSchool.Add("address", "'Sofia'");
            mockedSchool.Add("date_of_foundation","'" + DateTime.Now.ToString("yyyy-MM-dd") + "'");

            return mockedSchool;
        }

        [TestMethod]
        public void DataAdapter_ShouldAddNewStudent()
        {
            clearDb();

            IDataAdapter adapter = new DatabaseDataAdapter();
            adapter.SetSource(testDbConnectionString);


            //adapter.AddData(DbStructure.TEACHER_TABLE_NAME, new List<Dictionary<string, object>>() {getMockedTeacher() });
            //adapter.AddData(DbStructure.SCHOOL_TABLE_NAME, new List<Dictionary<string, object>>() { getMockedSchool() });
            //adapter.AddData(DbStructure.SCHOOL_CLASS_TABLE_NAME, new List<Dictionary<string, object>>() { getMockedClass() });

            adapter.AddData(DbStructure.STUDENT_TABLE_NAME, new List<Dictionary<string, object>>() { getMockedStudent() });

            List<Dictionary<string, object>> students = adapter.GetData(DbStructure.STUDENT_TABLE_NAME, new DataQuery { Id = -1 });

            Assert.AreEqual(1, students.Count);
        }

        [TestMethod]
        public void DataAdapter_ShouldDeleteStudent()
        {
            clearDb();

            IDataAdapter adapter = new DatabaseDataAdapter();
            adapter.SetSource(testDbConnectionString);

            adapter.AddData(DbStructure.STUDENT_TABLE_NAME, new List<Dictionary<string, object>>() { getMockedStudent() });
            adapter.DeleteData(DbStructure.STUDENT_TABLE_NAME, new DataQuery { Id = adapter.GetLastIdSaved(DbStructure.STUDENT_TABLE_NAME) });

            List<Dictionary<string, object>> students = adapter.GetData(DbStructure.STUDENT_TABLE_NAME, new DataQuery { Id = -1 });


            Assert.AreEqual(0, students.Count);

        }

        [TestMethod]
        public void DataAdapter_ShouldUpdateStudent()
        {
            clearDb();

            IDataAdapter adapter = new DatabaseDataAdapter();
            adapter.SetSource(testDbConnectionString);

            Dictionary<string,object> mocked = getMockedStudent();
            adapter.AddData(DbStructure.STUDENT_TABLE_NAME, new List<Dictionary<string, object>>() { mocked });

            mocked["first_name"] = "'Pesho'";
            adapter.UpdateData(DbStructure.STUDENT_TABLE_NAME, mocked, new DataQuery { Id = adapter.GetLastIdSaved(DbStructure.STUDENT_TABLE_NAME) });

            List<Dictionary<string, object>> students = adapter.GetData(DbStructure.STUDENT_TABLE_NAME, new DataQuery { Id = -1 });

            Assert.AreEqual(students[0]["first_name"], "Pesho");
        }
    }
}
