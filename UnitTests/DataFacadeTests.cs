﻿using DataAccessLayer;
using DatabaseConnectionLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace UnitTests
{
    [TestClass]
    public class DataFacadeTests
    {
        [TestMethod]
        public void DataFacade_ShouldAddStudent()
        {
            Student st = new Student();
            st.DateOfBirth = DateTime.Now;
            st.FirstName = "Ivan";
            st.MiddleName = "Ivanov";
            st.LastName = "Ivanov";
            st.ClassId = 1;

            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            Assert.IsTrue(dataFacade.AddObject<Student>(st, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns));
           
        }

        [TestMethod]
        public void DataFacade_IsStudentGotten()
        {
            Student st = new Student();
            st.DateOfBirth = new DateTime(2011, 2, 3);
            st.FirstName = "Ivan";
            st.MiddleName = "Ivanov";
            st.LastName = "Ivanov";
            st.ClassId = 1;

            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Student>(st, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            List<Student> result = dataFacade.GetTableData<Student>(new DataQuery { Id = -1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);
            Assert.AreEqual(1, result.Count);

        }

        [TestMethod]
        public void DataFacade_IsStudentDeleted()
        {
            Student st = new Student();
            st.DateOfBirth = new DateTime(2011, 2, 3);
            st.FirstName = "Ivan";
            st.MiddleName = "Ivanov";
            st.LastName = "Ivanov";
            st.ClassId = 1;

            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Student>(st, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);
            dataFacade.DeleteObject(new DataQuery { Id = 1 }, DbStructure.STUDENT_TABLE_NAME);

            List<Student> students = dataFacade.GetTableData<Student>(new DataQuery { Id = -1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);
            Assert.AreEqual(0, students.Count);
        }

        [TestMethod]
        public void DataFacade_IsStudentModified()
        {
            Student st = new Student();
            st.DateOfBirth = new DateTime(2011, 2, 3);
            st.FirstName = "Ivan";
            st.MiddleName = "Ivanov";
            st.LastName = "Ivanov";
            st.ClassId = 1;

            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Student>(st, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            st.FirstName = "Pesho"; 
            dataFacade.ModifyObject<Student>(st, new DataQuery { Id = 1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            List<Student> student = dataFacade.GetTableData<Student>(new DataQuery { Id = 1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            Assert.AreEqual(st.FirstName, student[0].FirstName);
        }

        [TestMethod]
        public void DataFacade_IsAddedObjectTheSame()
        {
            Student st = new Student();
            st.DateOfBirth = new DateTime(2011, 2, 3);
            st.FirstName = "Ivan";
            st.MiddleName = "Ivanov";
            st.LastName = "Ivanov";
            st.ClassId = 1;

            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Student>(st, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            List<Student> fetchedStudent = dataFacade.GetTableData<Student>(new DataQuery { Id = 1 }, DbStructure.STUDENT_TABLE_NAME, DbStructure.StudentColumns);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));
            string firstObj;
            string secondObj;

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, st);
                firstObj = textWriter.ToString();

                textWriter.GetStringBuilder().Clear();

                xmlSerializer.Serialize(textWriter, fetchedStudent[0]);
                secondObj = textWriter.ToString();
            }

            Assert.AreEqual(firstObj, secondObj);
        }
    }
}
